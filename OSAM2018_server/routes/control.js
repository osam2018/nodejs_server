var express = require('express');
var router = express.Router();
const db = require('../lib/db');
const jwt = require('jsonwebtoken');

router.get('/', (req, res) => {
	var html = ' ';
	db.query('select id, nickname, usertype from user where id=?', [req.id], (error, results, fields) => {
		if(error) return res.send(error);
		else if(!results[0]) return res.send("DB ERROR");
		else{
			html += `${results[0].nickname}님 환영합니다. ${req.id}로 로그인되었습니다. <h1>통합병력지원시스템</h1>`;
			if(results[0].usertype === 0){ // 관리자 1
				html += '체계 이용 권한이 없습니다. 관리자가 아닙니다.';
				return res.send(html);
			}
			html += '환영합니다. 통합병력지원시스템체계입니다.';
			db.query('select * from keycode', (error, results, fields) => {
				if(error) return res.send(error);
				else if(!results[0]) return res.send("DB ERROR");
				html += '<ul>';
				for(var i in results){
					html += '<li>' + results[i].keycode + '\t' + results[i].usertype + '\t' + results[i].created_time + '</li>';
				}
				html += '</ul>';
				html += 
`<form action="/control/add?token=${jwt.sign({id: req.id}, '2018osam!@#')}" method="post">
  <p> </p>
  <p>keycode</p>
  <input type="text" name="keycode" value="">
  <p>usertype</p>
	<div>
  <input type="radio" name="usertype" value="일반"> 일반
  <input type="radio" name="usertype" value="관리자"> 관리자
	</div>
  <p></p>
  <input type="submit" value="Submit">
  <input type="reset">
</form>`;
				return res.send(html);
			});
		}
	});
});

router.post('/add', (req, res) => {
	var keycode = req.body.keycode;
	var usertype = 0;
	if (req.body.usertype == "관리자")
		usertype = 1;
	
	db.query('INSERT INTO keycode (keycode, usertype, created_time) VALUES(?, ?, NOW())', [keycode, usertype], (error, results, fields) => {
		if(error) return res.send(error);
		else res.redirect('/control');
	});
});

module.exports = router;