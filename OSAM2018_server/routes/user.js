var express = require('express');
var router = express.Router();
const db = require('../lib/db');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;

router.get('/', function(req, res, next) {
  res.send('ok');
});

router.get('/info', (req, res) => {
	db.query('select id, nickname, keycode from user where id=?', [req.id], (error, results, fields) => {
				if(error) return res.json({isSuccess: false, message: error});
				else if (!results[0]) return res.json({isSuccess: false, message: "no data"});
				else res.send(Object.assign(results[0], {isSuccess: true, message: "ok"}));
			});
});

router.post('/change', (req, res) => {
	var id = req.body.id;
	var pw = req.body.pw;
	var pw_re = req.body.pw_re;
	var nickname = req.body.nickname;
	
	if(pw_re)
		if(pw != pw_re) return res.json({isSuccess: false, message: "비밀번호와 비밀번호 확인 값이 다릅니다."});
		else pw = bcrypt.hashSync(pw, saltRounds);
	db.query('select * from user where id=?', [req.id], (error, results, fields) => {
		if(error) return res.json({isSuccess: false, message: error});
		if(!results[0]) return res.json({isSuccess: false, message: "no data"});
		if(!id) id = req.id;
		if(!pw) pw = results[0].pw;
		if(!nickname) nickname = results[0].nickname;
		db.query('UPDATE user SET id = ?, pw = ?, nickname = ? WHERE id=?', [id, pw, nickname, req.id], (error, checks, fields) => {
			if(error) return res.json({isSuccess: false, message: error});
			else if(results[0].nickname != nickname)
				db.query('UPD0ATE article SET creator=? WHERE creator=?', [nickname, results[0].nickname], (error, results, fields) => {
					if(error) return res.json({isSuccess: true, sync: false, token: jwt.sign({id: id}, '2018osam!@#'), message: "사용자 정보는 변경되었지만 게시판 동기화에 실패하였습니다."});
					else return res.json({isSuccess: true, sync: true, token: jwt.sign({id: id}, '2018osam!@#'), message: "사용자 정보가 변경되었습니다."});
				});
			else return res.json({isSuccess: true, sync: true, token: jwt.sign({id: id}, '2018osam!@#'), message: "사용자 정보가 변경되었습니다."});
		});
	});
});

module.exports = router;
